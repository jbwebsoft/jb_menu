# Changelog

## 4.1.0
- prepares jb-menu for publishing

## 4.0.0
- updates angular to 4.0.0 as well as jb_responsive_breakpoints

## 2.2.0 
- added scss for internal component styling from the outside

## 2.0.0/2.0.1/2.1.0
- upgraded whole component to angular2

## 1.0.6
- ?

## 1.0.5
- fixed error in bound value mode in "active" attribute

## 1.0.4
- changed jb-menu dependencies to use https

## 1.0.3
- changed style a:hover and a.active in jb_menu_item to be a
  text shadow instead of font weight bold

## 1.0.2
- fixed error with "text" filed access on null object in JbMenu.onShadowRoot

## 1.0.1
- added ng-cloak class support to fix content transclusion of menu labels before redering

## 1.0.0
- first working version of jb-menu and jb-menu-item

## 0.0.1

- Initial version
