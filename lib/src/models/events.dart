library jb_menu.events;

import 'dart:html';

/// event names
const String closeEventName = "close-menu";
const String toggleMenuEventName = "toggle-menu";
const String collapseEventName = "collapse-event";
const String uncollapseEventName = "uncollapse-event";

/// events
final CustomEvent closeEvent = new CustomEvent(closeEventName);
final CustomEvent toggleMenuEvent = new CustomEvent(toggleMenuEventName);
final CustomEvent collapseEvent = new CustomEvent(collapseEventName);
final CustomEvent uncollapseEvent = new CustomEvent(uncollapseEventName);