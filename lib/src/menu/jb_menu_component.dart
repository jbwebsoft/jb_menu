library jb_menu.jb_menu_component;


import 'package:angular/angular.dart';
import 'package:jb_menu/src/item/jb_menu_item.dart';
import 'package:jb_menu/src/models/events.dart';
import 'package:logging/logging.dart';

@Component(
    selector: 'jb-menu [collapsedStateBreakpoints] [activeBreakpoints] ',
    templateUrl: 'jb_menu_component.html',
    directives: const [CORE_DIRECTIVES, JbMenuItem],
    styleUrls: const ["jb_menu_component.css"]
)
class JbMenu implements AfterViewInit, AfterContentInit {

  final Logger _log = new Logger("jb_menu.jb_menu_component");

  /// a list with all breakpoint names in which the menu should be collapsed
  /// should be bound from a RootScope Variable
  @Input()
  List collapsedStateBreakpoints;

  String _operationMode = "responsive";

  /// Label for collapsed menu button
  @Input()
  String labelCollapsed = "Menü";

  /// Defines, whether the component is currently collapsed or not
  @Input()
  bool isOpen = true;

  /// Defines, whether the menu is collapsible or not
  @Input()
  bool isCollapsible = false;

  List<String> _activeBreakpoints;

  @ContentChildren(JbMenuItem)
  QueryList<JbMenuItem> menuItems;


  JbMenu() {}

  get activeBreakpoints {
    return _activeBreakpoints;
  }

  @Input()
  set activeBreakpoints(List<String> breakpoints) {
    _log.finest("ActiveBreakpoints list changed");
    _activeBreakpoints = breakpoints;
    var breakpoint = (breakpoints.length > 0) ? breakpoints.last : null;

    if (breakpoint == null || _operationMode == "static") return;
    _log.finest("Current Breakpoint: $breakpoint");

    if (collapsedStateBreakpoints.contains(breakpoint)) {
      //activate collapsible on small layouts
      _log.finest("send $collapseEventName");
      toggleCollapsible(true);
    } else {
      //deactivate collapsible on big layouts
      _log.finest("send $uncollapseEventName");
      toggleCollapsible(false);
    }
  }


  /// menu-mode parameter:
  ///  static - not collapsing
  ///  responsive - collapsing when currentBreakpoint is in collapsedBreakpoints
  @Input()
  set operationMode(String value) {
    _log.fine("OperationMode set: $value");
    _operationMode = value;
    switch (value) {
      case "static":
        toggleCollapsible(false);
        break;
      case "responsive":
        toggleCollapsible(true);
        break;
      default:
        toggleCollapsible(true);
        break;
    }
  }

  @override
  ngAfterViewInit() {
    _log.finest(
        ''' AfterViewInit:
        collapsedStateBreakpoints: $collapsedStateBreakpoints,
        _operationMode: $_operationMode,
        labelCollapsed = $labelCollapsed,
        isOpen: $isOpen,
        isCollapsible: $isOpen
        '''
    );
  }


  void toggleCollapsible(bool activate) {
    isCollapsible = activate;
    isOpen = !activate;
  }


  ///gives the menu button the functionality to toggle the menu,
  ///gives links the ability to close the menu when they are clicked
  void toggleMenu(dynamic data) {
    if (isCollapsible) {
      var lastOpenState = isOpen;
      isOpen = !isOpen;
      _log.info("Menu state changed from ${lastOpenState ? "open" : "closed"} to ${isOpen ? "open" : "closed"}");
    } else {
      _log.info("Menu state not changed because isCollapsible is false");
    }
  }

  @override
  ngAfterContentInit() {
    menuItems.forEach((menuItem) {
      menuItem.onToggleMenu.listen(toggleMenu);
    });
  }
}
