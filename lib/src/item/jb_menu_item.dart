library jb_menu.jb_menu_item;

import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:jb_menu/src/models/events.dart';
import 'package:logging/logging.dart';

@Component(
    selector: 'jb-menu-item',
    directives: const [CORE_DIRECTIVES],
    templateUrl: 'jb_menu_item.html',
    styleUrls: const ["jb_menu_item.css"])
class JbMenuItem implements AfterViewInit, OnDestroy {
  final Logger _logger = new Logger("jb_menu.jb_menu_item");

  @Input()
  String url = "";

  @Input()
  bool active = false;

  @Input()
  bool collapsedMode = false;

  @Input()
  bool isMenuButton = false;

  @Input()
  bool disableNavigation = false;

  @Input()
  bool openInNewTab = false;

  final StreamController _onCloseMenu = new StreamController.broadcast();

  @Output()
  Stream get onCloseMenu => _onCloseMenu.stream;

  final StreamController _onToggleMenu = new StreamController.broadcast();

  @Output()
  Stream get onToggleMenu => _onToggleMenu.stream;

  JbMenuItem() {
    _logger.info("JbMenuItem constructor: ${this.hashCode}");
  }

  String get target => (openInNewTab) ? "_blank" : "_self";

  @override
  ngAfterViewInit() {
    _logger.finest(''' AfterViewInit for $hashCode:
        isMenuButton: $isMenuButton,
        url: $url,
        active: $active,
        collapsedMode: $collapsedMode,
        openInNewTab: $openInNewTab,
        disableNavigation: $disableNavigation
        ''');
  }

  void onClick(Event event) {
    toggleMenu();

    if (disableNavigation || isMenuButton) event.preventDefault();
  }

  void toggleMenu() {
    _onToggleMenu.add(toggleMenuEvent);
  }

  @override
  ngOnDestroy() {
    _onCloseMenu.close();
    _onToggleMenu.close();
  }
}
