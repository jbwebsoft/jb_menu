# jb_menu

An Angular Component for easy responsive menus (was started before angular material).

## Usage
1. Add the component module to your pubspec.yaml with git and specific branch reference  

    ```
    # update this with current version
    jb_menu: "^1.0.0" "
    ```

2. Show this module to angular dependency injection -> see App Class in app_initialize.dart

        import 'package:jb_menu/jb_menu.dart';

        class App extends Angular.Module {

          App() {
            install(JbMenuModule);
          }
        }
        
## Options for jb-menu
| Option Name | Option Values | Required | Desctiption |
| ----------- | ------------- | ----------- | --------- |
| operation-mode  | static or responsive | required | sets, whether jb-menu reacts to breakpoint changes or not |
| label-collapsed | string, default "Menü" | optional | sets the text on the menu button in collapsed state |
| is-open | bool, default false | optional | sets the initial state of the menu to opened, if no other option prohibits this |
| collapsed-state-breakpoints | list of breakpoint names from root context| if not set = operation-mode="static" | 
breakpoints where the menu should be collapsed 
| current-breakpoint | string, two-way-bindable | required for operation-mode="responsive" | should be bound to <jb-responsive-breakpoints current-breakpoint="[var]"> attribute 
        
## Change internal styles:

1. Add jb-style tag somewhere
2. Add content attribute to it (two way bindable var)
3. Add this two way variable to the style attribute of jb-menu or to each jb-menu-item tag 
   to inject the style into their shadow dom
  
Internal styles in jb-menu:
  nav {
          display: inline-flex;
          flex-flow: row nowrap;
          align-items: stretch;
      }
        
  nav.collapsed {
      width: 100%;
      flex-flow: column nowrap;
      align-items: stretch;
  }
    
Internal styles in jb-menu-item:
  * {
        box-sizing: border-box;
    }
  
    a {
        /* text-settings */
        text-decoration: none;
        font-family: 'Roboto', sans-serif;
        font-style: normal;
        font-size: 1.5em;
        font-weight: 300;
        /*text-shadow: #666666 1px 1px 5px;*/
        color: #000000;
        /*color: #5e5e5e;*/
   
        margin: 5px;
        padding: 5px;
    
        cursor: pointer;
    }
    
    a.collapsed {
        text-align: center;
    }
    
    a:hover, .active {
        font-weight: bold;
        padding-left: 3px;
        padding-right: 2px;
        /*text-shadow: rgb(180, 180, 180) 1px 1px 10px;*/
    }