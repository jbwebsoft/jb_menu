library jb_menu.app_init;

import 'package:angular/angular.dart';

import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';

//import own components
import 'app_component.dart';

final Logger _libLogger = new Logger("jb_menu");

void main() {
  //init logging
  hierarchicalLoggingEnabled = true;
  Logger.root.onRecord.listen(new LogPrintHandler());

  Logger.root.level = Level.OFF;
  _libLogger.level = Level.ALL;

  bootstrap(AppComponent);
}
