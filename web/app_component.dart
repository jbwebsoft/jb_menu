import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:jb_responsive_breakpoints/jb_responsive_breakpoints.dart';
import 'package:jb_menu/jb_menu.dart';

@Component(
    selector: 'app-root',
    templateUrl: 'app_component.html',
    styleUrls: const ["css/app_styles.css"],
    directives: const [CORE_DIRECTIVES, JbResponsiveBreakpoints, JbMenu, JbMenuItem])
class AppComponent {

  DomSanitizationService sanitizationService;

  AppComponent(this.sanitizationService);

  Map<int, String> breakpoints = {0: 'smallest', 300: 'small', 500: 'medium', 900: 'large', 1200: 'xlarge'};

  List<String> activeBreakpoints = new List<String>();

  List collapsedStateBreakpoints = ["smallest", "small", "medium"];

  String url = "http://www.w3schools.com/";

  //TODO: find secure solution
  SafeResourceUrl get sanitizedUrl => sanitizationService.bypassSecurityTrustResourceUrl(url);

  Map menuItems = {
    "http://www.w3schools.com/": "W3C Schools",
    "http://stadt-bremerhaven.de/": "Caschys Blog",
    "http://www.chip.de/#": "Chip"
  };

  Map footerItems = {
    "http://www.ikea.com/de/de/?cid=1RMdPGnRaHZDXiaizr5eeUzANjX7z5": "Ikea",
    "http://www.linusmediagroup.com/": "Linus Tech Tipps",
    "https://winfuture.de": "Winfuture",
  };

  bool isActiveUrl(String testUrl) => url == testUrl;

  void onClick(String newUrl) {
    url = newUrl;
  }
}